class LivroDao {
  constructor (db) {
    this._db = db;
  }

  create (livro) {
    return new Promise((resolve, reject) => {
      this._db.run(`
            INSERT INTO LIVROS (
                    titulo,
                    preco,
                    descricao
                ) values (?, ?, ?)
            `,
      [
        livro.titulo,
        livro.preco,
        livro.descricao
      ],
      function (err) {
        if (err) {
          console.log(err);
          return reject('Não foi possível adicionar o livro!');
        }

        resolve();
      }
      );
    });
  }

  getId (id) {
    console.log(id);
    return new Promise((resolve, reject) => {
      this._db.get(
        'SELECT * FROM livros WHERE id = ?', id,
        (error, result) => {
          if (error) return reject('Não foi Possivel listr os livros!');

          return resolve(result);
        }
      );
    });
  }

  list () {
    return new Promise((resolve, reject) => {
      this._db.all(
        'SELECT * FROM livros',
        (error, results) => {
          if (error) return reject('Não foi Possivel listr os livros!');

          return resolve(results);
        }
      );
    });
  }

  update (livro) {
    return new Promise((resolve, reject) => {
      this._db.run(`
            UPDATE livros SET
            titulo = ?,
            preco = ?,
            descricao = ?
            WHERE id = ?
        `,
      [
        livro.titulo,
        livro.preco,
        livro.descricao,
        livro.id
      ],
      erro => {
        if (erro) {
          return reject('Não foi possível atualizar o livro!');
        }

        resolve();
      });
    });
  }

  delete (id) {
    return new Promise((resolve, reject) => {
      this._db.get(
        `
                DELETE 
                FROM livros
                WHERE id = ?
            `,
        [id],
        (erro) => {
          if (erro) {
            return reject('Não foi possível remover o livro!');
          }
          return resolve();
        }
      );
    });
  }
}

module.exports = LivroDao;
