const LivroDao = require('../infra/livro-dao');
const db = require('../../config/database');

module.exports = (app) => {
  app.get('/', function (req, resp) {
    resp.send(
      `
                <html>
                    <head>
                        <meta charset="utf-8">
                    </head>
                    <body>
                        <h1> Casa do Código </h1>
                    </body>
                </html>
            `
    );
  });

  app.get('/livros', function (req, resp) {
    const livroDao = new LivroDao(db);

    livroDao.list()
      .then(livros => resp.marko(
        require('../views/livros/lista/list.marko'), {
          livros: livros
        }
      ))
      .catch(error => console.log(error));
  });

  app.get('/livros/form/:id', function (req, resp) {
    const id = req.params.id;
    const livroDao = new LivroDao(db);

    livroDao.getId(id)
      .then(livro => resp.marko(
        require('../views/livros/forms/form.marko'),
        { livro: livro }
      ))
      .catch(error => console.log(error));
  });

  app.delete('/livros/:id', function (req, resp) {
    const id = req.params.id;

    const livroDao = new LivroDao(db);
    livroDao.delete(id)
      .then(() => resp.status(200).end())
      .catch(erro => console.log(erro));
  });

  app.get('/livros/form', function (req, resp) {
    resp.marko(require('../views/livros/forms/form.marko'), { livro: {} });
  });

  app.post('/livros', function (req, resp) {
    console.log(req.body);
    const livroDao = new LivroDao(db);

    livroDao.create(req.body)
      .then(resp.redirect('/livros'))
      .catch(error => console.log(error));
  });

  app.put('/livros', function (req, resp) {
    const livroDao = new LivroDao(db);

    livroDao.update(req.body)
      .then(resp.redirect('/livros'))
      .catch(erro => console.log(erro));
  });
};
